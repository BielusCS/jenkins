JENKINS MANUAL


>> Instal·lació (sota Debian GNU/Linux) <<

    a) Obtenir la clau GPG (des de terminal)

        /* Es descarrega la clau i s'afegeix al sistema */

        wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

    b) Afegir el repositori (des de terminal)

        /* Es crea el fitxer que contindrà la URL del repositori */

        sudo vim /etc/apt/sources.list.d/jenkins.list

            deb https://pkg.jenkins.io/debian-stable binary/
    
    c) Actualitzar l'índex local de paquets i instal·lar Jenkins (des de terminal)

        sudo apt-get update
        sudo apt-get install jenkins


>> Configuració Inicial <<

    a) Carregar la interfície web (des del navegador)

        /* URL per defecte de Jenkins */

        localhost:8080
    
    b) Inserir el password inicial d'admin generat per Jenkins

        /* Des de terminal, es consulta el password */

        sudo cat /var/lib/jenkins/secrets/initialAdminPassword

    c) Personalitzar Jenkins

        /* Es selecciona l'opció suggerida perquè es tracta de la instal·lació inicial sense coneixements previs */

        Opció --> Install suggested plugins
    
    d) Crear el primer usuari Administrador

        /* Es crea l'usuari administrador amb el qual es gestionarà Jenkins sota l'ordinador actual */

        Username --> usuari
        Password --> ***********
        Full name --> Usuari
        E-mail address --> bielo85@gmail.com

    e) Configuració de la instància

        /* URL de Jenkins quan arrenqui. En aquest cas, no varia perquè es treballa en local */

        Jenkins URL --> http://localhost:8080/


>> Actualitzar Jenkins <<

    a) Actualitzar l'índex local de paquets (des de terminal)
    
        sudo apt-get update
    
    b) Actualitzar Jenkins
    
        sudo apt-get upgrade


>> Crear un Projecte de software <<

    a) Preparació de l'entorn (des de terminal)

        /* Instal·lació de Git */
        
        sudo apt-get install git

        /* Creació del repositori */

        mkdir JenkinsProjects && cd JenkinsProjects/
        mkdir JavaApp_CI_01 && cd JavaApp_CI_01/
        git init

    b) Dashboard

        Opció --> New Item
    
    c) Configuració del Job (Part 1)

        Item name --> Hello World
        Project type --> Freestyle project

    d) Configuració del Job (Part 2)

        Description --> Hello world java test program
        Source Code Management --> Git
        Repository URL --> /home/usuari/JenkinsProjects/JavaApp_CI_01/
        Credentials --> none
    
    e) Configuració del Job (Part 3)

        Add build step --> Execute shell
        Command --> javac HelloWorld.java
                    java HelloWorld
        Button 'Apply' --> prémer
        Button 'Save' --> prémer




>> Role-based Authorization Strategy <<

    a) Carregar el gestor de Jenkins (des del dashboard)

        Opció --> Manage Jenkins
    
    b) Carregar el gestor de Plugins (des del dashboard)

        Opció --> Manage Plugins
    
    c) Cercar el plugin

        Tab --> Available
        Search --> role
    
    d) Instal·lar el plugin

        Check 'Install' --> seleccionar "Role-based Authorization Strategy"
        Button 'Install without restart' --> prémer
        Link 'Go back to the top page' --> prémer
    
    e) Activar el plugin (des del dashboard)

        Opció --> Manage Jenkins
        Opció --> Configure Global Security (apartat 'Security')
        Opció 'Role-Based Strategy' --> seleccionar (apartat 'Authorization')
        Button 'Save' --> prémer


>> Gestionar Usuaris i Rols - Crear Usuaris <<

    a) Carregar el gestor de Jenkins (des del dashboard)

        Opció --> Manage Jenkins
    
    b) Gestionar Usuaris

        Opció --> Manage Users
    
    c) Crear nou usuari

        Opció --> Create User
        
        Username --> usuari080401
        Password --> **********
        Full name --> Usuari 080401
        E-mail address --> bielo.85@gmail.com

        Button 'Create User' --> prémer


>> Gestionar Usuaris i Rols - Crear Rols <<

    a) Carregar el gestor de Jenkins (des del dashboard)

        Opció --> Manage Jenkins
    
    b) Gestionar Rols

        Opció --> Manage and Assign Roles
    
    c) Afegir un nou Rol

        Opció --> Manage Roles
        Role to add --> developer
        Button 'Add' --> prémer
    
    d) Especificar els permisos del nou rol

        Credentials --> Create, Delete, Update, View
        Button 'Save' --> prémer


>> Gestionar Usuaris i Rols - Assignar Rols <<

    a) Carregar el gestor de Jenkins (des del dashboard)

        Opció --> Manage Jenkins
    
    b) Gestionar Rols

        Opció --> Manage and Assign Roles

    c) Assignar un Rol

        Opció --> Assign Roles

        User/group to add --> usuari080401
        Button 'Add' --> prémer

        Check 'developer' --> seleccionar per user 'usuari080401'
        Button 'Save' --> prémer


>> Gestionar Usuaris i Rols - Crear Rols de Projecte <<

    a) Carregar el gestor de Jenkins (des del dashboard)

        Opció --> Manage Jenkins
    
    b) Gestionar Rols

        Opció --> Manage and Assign Roles
    
    c) Afegir un nou Rol (a la taula 'Item roles')

        Opció --> Manage Roles
        Role to add --> tester
        Pattern --> tester.*
        Button 'Add' --> prémer
    
    d) Especificar els permisos del nou rol

        Credentials --> Delete
        Job --> Configure, Read
        Button 'Save' --> prémer




>> Bibliografia <<

    * https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/jenkins-tutorial/
    * https://www.jenkins.io/download/
    * https://en.wikipedia.org/wiki/Jenkins_(software)
    * https://www.guru99.com/jenkins-tutorial.html
    * https://www.guru99.com/continuous-integration.html
    * https://www.guru99.com/create-users-manage-permissions.html

